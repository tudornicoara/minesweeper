﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MineSweeper
{
    class Block
    {
        public int Size { get; set; }
        public int PositionX { get; set; }
        public int PositionY { get; set; }
        public Utilities.BlockStates BlockState { get; set; }
        public Utilities.BlockVisibleStates BlockVisibleState { get; set; }

    public Block(int size, int positionX, int positionY)
        {
            Size = size;
            PositionX = positionX;
            PositionY = positionY;

            Random rand = new();
            BlockState = (Utilities.BlockStates)rand.Next(2);

            BlockVisibleState = 0;
        }
    }
}
