﻿using SFML.Graphics;
using SFML.System;
using SFML.Window;
using System;
using System.Collections;
using System.Collections.Generic;

namespace MineSweeper
{
    class Game
    {
        private readonly RenderWindow _window;
        private readonly CircleShape _player = new();
        private List<Block> Blocks = new List<Block>();
        private int SizeX = 800;
        private int SizeY = 800;

        public Game()
        {
            _window = new RenderWindow(new VideoMode((uint)SizeX, (uint)SizeY), "SFML Application");
            _window.Closed += new EventHandler(OnClose);
            _window.KeyPressed += new EventHandler<KeyEventArgs>(KeyHandler);
            _player.Radius = 120.0f;
            _player.Position = new Vector2f(400.0f - _player.Radius, 400.0f - _player.Radius);
            _player.FillColor = Color.Cyan;
        }


        public void Run()
        {
            Setup();

            while (_window.IsOpen)
            {
                ProcessEvents();
                Update();
                Render();
            }
        }

        private void Setup()
        {
            int blocSize = 20;
            int numBlocs = SizeX * SizeY / blocSize;
            int numX = SizeX / blocSize;
            int numY = SizeY / blocSize;

            for (int i = 0; i < numX; i++)
            {
                for (int j = 0; j < numY; j++)
                {
                    int positionX = i % numX * blocSize;
                    int positionY = j % numY * blocSize;

                    Block block = new Block(blocSize, positionX, positionY);
                    Blocks.Add(block);
                }
            }

            //Block block = new Block(blocSize, 0, 0);
            //Blocks.Add(block);
        }

        private void KeyHandler(object sender, KeyEventArgs e)
        {

            if (e.Code == Keyboard.Key.Right)
            {
                Random rand = new();
                int red = rand.Next(256);
                int green = rand.Next(256);
                int blue = rand.Next(256);

                this._player.FillColor = new Color((byte)red, (byte)green, (byte)blue);
            }

            if (e.Code == Keyboard.Key.Escape)
            {
                this._window.Close();
            }

        }

        static void OnClose(object sender, EventArgs e)
        {
            RenderWindow window = (RenderWindow)sender;
            window.Close();
        }

        private void ProcessEvents()
        {

            _window.DispatchEvents();

        }

        private void Update()
        {
        }

        private void Render()
        {
            _window.Clear();
            _window.Draw(_player);

            int i = 0;
            foreach (var block in Blocks)
            {
                RectangleShape rectangle = new RectangleShape()
                {
                    Size = new Vector2f(block.Size, block.Size),
                    Position = new Vector2f(block.PositionX, block.PositionY),
                    FillColor = Color.White,
                    OutlineThickness = 2,
                    OutlineColor = Color.Black
                };
                _window.Draw(rectangle);
                Text text = new Text
                {
                    DisplayedString = i.ToString(),
                    CharacterSize = 8,
                    FillColor = Color.Red,
                    Position = new Vector2f(block.PositionX, block.PositionY)
                };

                i++;
            }

            _window.Display();
        }
    }
}
