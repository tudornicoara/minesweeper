﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MineSweeper
{
    class Utilities
    {
        public enum BlockVisibleStates
        {
            Empty,
            Unpressed,
            Flag,
            Bomb,
            RedBomb,
            One,
            Two,
            Three,
            Four,
            Five,
            Six,
            Seven,
            Eight,
            Nine
        }

        public enum BlockStates
        {
            NotBomb,
            Bomb
        }
    }
}
